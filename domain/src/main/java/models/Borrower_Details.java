package models;

import javax.persistence.*;

@Entity
@Table(name = "borrower_details")
public class Borrower_Details {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 11, nullable = false)
    private Integer id_borrower_details;

    @Column()
    private String address;
    private String email;
    private String phone;
}
