package models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "borrow")
public class Borrow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 11, nullable = false)
    private Integer id_borrow;

    @Column(nullable = false)
    private Date rental_date;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book_id;

    @ManyToOne
    @JoinColumn(name = "borrow_id")
    private Borrow borrow_id;

    public Integer getId_borrow() {
        return id_borrow;
    }

    public void setId_borrow(Integer id_borrow) {
        this.id_borrow = id_borrow;
    }

    public Date getRental_date() {
        return rental_date;
    }

    public void setRental_date(Date rental_date) {
        this.rental_date = rental_date;
    }

    public Book getBook_id() {
        return book_id;
    }

    public void setBook_id(Book book_id) {
        this.book_id = book_id;
    }

    public Borrow getBorrow_id() {
        return borrow_id;
    }

    public void setBorrow_id(Borrow borrow_id) {
        this.borrow_id = borrow_id;
    }
}
