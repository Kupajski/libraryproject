package models;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name="author")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 11, nullable = false)
    private Integer id_author;

    @Column(length = 255, nullable = false)
    private String first_name;
    private String last_name;

    @Column(nullable = true)
    private String birth_place;

//    @OneToMany(mappedBy = "book_id")
//    private Set<Book> book_id;

    public Author() {
    }

    public Author(String first_name, String last_name) {
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public Integer getId_author() {
        return id_author;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getBirth_place() {
        return birth_place;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setBirth_place(String birth_place) {
        this.birth_place = birth_place;
    }
}
