package models;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;
import java.util.Set;


@Entity
@Table(name="book")
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 11, nullable = false)
    private Integer id_book;

    @Column(nullable = false)
    private String isbn;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String pages;

    @Column()
    private String release_date;
    private String summary;
    private Boolean borrow;
    private String category;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author_id;

//    @OneToMany(mappedBy = "borrowers")
//    private Set<Borrow> borrowers;


    public Book() {
    }

    public Book(String isbn, String title, String pages, String release_date, String summary, Boolean borrow, String category, Author author_id) {
        this.isbn = isbn;
        this.title = title;
        this.pages = pages;
        this.release_date = release_date;
        this.summary = summary;
        this.borrow = borrow;
        this.category = category;
        this.author_id = author_id;
    }

    public Integer getId_book() {
        return id_book;
    }

    public void setId_book(Integer id_book) {
        this.id_book = id_book;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Boolean getBorrow() {
        return borrow;
    }

    public void setBorrow(Boolean borrow) {
        this.borrow = borrow;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Author getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(Author author_id) {
        this.author_id = author_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(id_book, book.id_book) &&
                Objects.equals(isbn, book.isbn) &&
                Objects.equals(title, book.title) &&
                Objects.equals(pages, book.pages) &&
                Objects.equals(release_date, book.release_date) &&
                Objects.equals(summary, book.summary) &&
                Objects.equals(borrow, book.borrow) &&
                Objects.equals(category, book.category) &&
                Objects.equals(author_id, book.author_id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id_book, isbn, title, pages, release_date, summary, borrow, category, author_id);
    }
}
