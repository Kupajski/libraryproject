package models;


import javax.persistence.*;


@Entity
@Table(name = "borrower")
public class Borrower {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 11, nullable = false)
    private Integer id_borrower;

    @Column(nullable = false)
    private String login;

    @Column(nullable = false )
    private String password;

    @OneToOne
    @JoinColumn(name = "borrower_details_id")
    private Borrower_Details borrower_details_id;

    public Borrower() {
    }

    public Borrower(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Integer getId_borrower() {
        return id_borrower;
    }

    public void setId_borrower(Integer id_borrower) {
        this.id_borrower = id_borrower;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Borrower_Details getBorrower_details_id() {
        return borrower_details_id;
    }

    public void setBorrower_details_id(Borrower_Details borrower_details_id) {
        this.borrower_details_id = borrower_details_id;
    }
}
