package services;

import models.Author;
import models.Book;
import repositories.AuthorDao;
import repositories.BookDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

import static java.lang.Integer.parseInt;

public class BookService {

    private static BookService instance;
    private EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
    private EntityManager entityManager = managerFactory.createEntityManager();
    private BookDao bookDao = new BookDao(entityManager);


    public static BookService getInstance(){
        if(instance == null){
            instance = new BookService();
            return instance;
        }
        return instance;
    }
    public List<Book> getAllBooks(){

        List<Book> list = bookDao.findAll();

        return list;
    }
    public Book addBook(String title, String Category, String first_name,String last_name, String ISBN, String Release_date, String Pages, String Summary){

        AuthorDao authorDao = new AuthorDao(entityManager);
        Author author = authorDao.findAuthor(first_name,last_name);

        //TODO Parsowanie daty

        if(author==null){

            Author newAuthor = authorDao.create(new Author(first_name,last_name));
            Book createdBook = bookDao.create(
                    new Book(ISBN,title,Pages,Release_date,Summary,false,Category, newAuthor)
            );

            return createdBook;
        }else{

            Book createdBook = bookDao.create(
                    new Book(ISBN,title,Pages,Release_date,Summary,false,Category, author)
            );
            return createdBook;
        }
    }

    public Book getBook(Integer book_id){
        return bookDao.read(book_id);
    }

    public void updateBook(String book_id, String title, String Category, String ISBN, String Release_date, String Pages, String Summary,String first_name,String last_name){

        Integer bookId = Integer.valueOf(book_id);
        Book book = getBook(bookId);
        AuthorDao authorDao = new AuthorDao(entityManager);
        Author author = authorDao.findAuthor(first_name,last_name);

        book.setTitle(title);
        book.setCategory(Category);
        book.setIsbn(ISBN);
        book.setRelease_date(Release_date);
        book.setPages(Pages);
        book.setSummary(Summary);
        book.setAuthor_id(author);

        bookDao.update(book);

    }

    public void deleteBook(Book book){
        bookDao.delete(book);
    }
}
