package services;

import models.Borrower;
import repositories.BorrowerDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class UserService {

    private static UserService instance;
    private EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
    private EntityManager entityManager = managerFactory.createEntityManager();
    private BorrowerDao borrowerDao = new BorrowerDao(entityManager);

    public static UserService getInstance(){
        if(instance==null){
            instance = new UserService();
            return instance;
        }
        return instance;
    }

    public boolean findByLogin(String login){

         return borrowerDao.findByLogin(login);
    }

    public Borrower getUser(Integer user_id){
        return borrowerDao.read(user_id);
    }

    public void createUser(String login, String password){

        borrowerDao.create(new Borrower(login,password));

    }

    public void loginUser(String login, String password){

    }
}
