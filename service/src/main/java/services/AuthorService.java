package services;

import models.Author;
import repositories.AuthorDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AuthorService {

    private static AuthorService instance;
    private EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
    private EntityManager entityManager = managerFactory.createEntityManager();
    private AuthorDao authorDao = new AuthorDao(entityManager);

    public static AuthorService getInstance(){
        if(instance == null){
            instance = new AuthorService();
            return instance;
        }
        return instance;
    }
}
