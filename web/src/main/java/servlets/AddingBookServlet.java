package servlets;

import services.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlets.addingBookServlet")
public class AddingBookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("addingBook.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch(req.getParameter("action")){
            case "add":{
                String bookTitle = req.getParameter("bookTitle");
                String bookCategory = req.getParameter("bookCategory");
                String firstName = req.getParameter("first_name");
                String lastName = req.getParameter("last_name");
                String isbn = req.getParameter("isbn");
                String bookReleaseDate = req.getParameter("bookReleaseDate");
                String bookPages = req.getParameter("bookPages");
                String bookSummary = req.getParameter("bookSummary");

                BookService.getInstance().addBook(bookTitle,bookCategory,firstName,lastName,isbn,bookReleaseDate,bookPages,bookSummary);
                resp.sendRedirect("servlets.HomeServlet");
                break;
            }
            case "cancel":{
                req.getRequestDispatcher("index.jsp").forward(req,resp);
                break;
            }
        }

    }
}
