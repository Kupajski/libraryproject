package servlets;

import models.Book;
import services.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlets.showingBookServlet")
public class ShowingBookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String book_id = req.getParameter("book_id");
        Integer bookId = Integer.valueOf(book_id);
        Book book = BookService.getInstance().getBook(bookId);
        req.setAttribute("book",book);
        req.getRequestDispatcher("showingBook.jsp").forward(req,resp);
    }
}
