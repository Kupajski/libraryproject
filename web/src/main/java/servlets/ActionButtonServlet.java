package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlets.ActionButtonServlet")
public class ActionButtonServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("index.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String checkboxBookChoice = req.getParameter("checkboxBookChoice");
        switch(req.getParameter("action")){
            case "add": {
                resp.sendRedirect("/servlets.addingBookServlet");
                break;
            }
            case "edit": {
                resp.sendRedirect("/servlets.editingBookServlet?book_id=" + checkboxBookChoice);
                break;
            }
            case "delete": {
                resp.sendRedirect("/servlets.deletingBookServlet?book_id=" + checkboxBookChoice);
                break;
            }
            case "show": {
                resp.sendRedirect("/servlets.showingBookServlet?book_id=" + checkboxBookChoice);
                break;
            }
        }
    }
}
