package servlets;

import models.Book;
import services.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlets.deletingBookServlet")
public class DeletingBookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bookIdParameter = req.getParameter("book_id");
        Integer bookId = Integer.valueOf(bookIdParameter);
        Book book = BookService.getInstance().getBook(bookId);

        BookService.getInstance().deleteBook(book);
        resp.sendRedirect("servlets.HomeServlet");
    }


}
