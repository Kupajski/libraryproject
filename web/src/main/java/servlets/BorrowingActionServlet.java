package servlets;

import models.Book;
import services.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlets.borrowingActionServlet")
public class BorrowingActionServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bookIdStr = req.getParameter("idBookBorrow");
        Integer bookId = Integer.valueOf(bookIdStr);
        Book book =BookService.getInstance().getBook(bookId);
        book.setBorrow(true);
        resp.sendRedirect("/servlets.showingBookServlet?book_id=" + bookIdStr);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bookIdStr = req.getParameter("idBookReturn");
        Integer bookId = Integer.valueOf(bookIdStr);
        Book book =BookService.getInstance().getBook(bookId);
        book.setBorrow(false);
  //      req.getRequestDispatcher("/servlets.borrowingActionServlet").forward(req,resp);
        resp.sendRedirect("/servlets.showingBookServlet?book_id=" + bookIdStr);
    }
}
