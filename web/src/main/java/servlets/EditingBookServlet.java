package servlets;

import models.Book;
import services.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlets.editingBookServlet")
public class EditingBookServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bookIdParameter = req.getParameter("book_id");
        Integer bookId = Integer.valueOf(bookIdParameter);
        Book editBook = BookService.getInstance().getBook(bookId);
        req.setAttribute("book",editBook);
        req.getRequestDispatcher("editingBook.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        switch(req.getParameter("action")){
            case "edit":{

                String bookId = req.getParameter("id_book");
                String bookTitle = req.getParameter("bookTitle");
                String bookCategory = req.getParameter("bookCategory");
                String firstName = req.getParameter("first_name");
                String lastName = req.getParameter("last_name");
                String isbn = req.getParameter("isbn");
                String bookReleaseDate = req.getParameter("bookReleaseDate");
                String bookPages = req.getParameter("bookPages");
                String bookSummary = req.getParameter("bookSummary");

                BookService.getInstance().updateBook(bookId,bookTitle,bookCategory,isbn,bookReleaseDate,bookPages,bookSummary,firstName,lastName);
                req.getRequestDispatcher("servlets.HomeServlet").forward(req,resp);

            }
            case "cancel":{
                req.getRequestDispatcher("index.jsp").forward(req,resp);
            }
        }
    }
}
