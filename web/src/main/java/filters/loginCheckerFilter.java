package filters;

import services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/registerUserServlet")
public class loginCheckerFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        String login = httpServletRequest.getParameter("login");


        if(!UserService.getInstance().findByLogin(login)){
            filterChain.doFilter(servletRequest,servletResponse);
        }else{
            String warning = "Login zajety";
            httpServletResponse.sendRedirect("/register.jsp?warning=" + warning);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
