package filters;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/registerUserServlet")
public class correctPasswordFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        String password = httpServletRequest.getParameter("password");
        String passwordChecker =httpServletRequest.getParameter("passwordChecker");

        if(password.equals(passwordChecker)){
            filterChain.doFilter(servletRequest,servletResponse);
        }else{
          String warning = "Zle haslo";
          httpServletResponse.sendRedirect("/register.jsp?warning=" + warning);
        }
    }

    @Override
    public void destroy() {

    }

}
