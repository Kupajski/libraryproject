<%--
  Created by IntelliJ IDEA.
  User: Bartosz
  Date: 16.09.2018
  Time: 12:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Adding Book</title>
    <link rel="stylesheet" href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
<jsp:include page="WEB-INF/header.jsp"/>
<form action="/servlets.addingBookServlet" method="POST" style="margin-top: 100px">
    <jsp:include page="WEB-INF/formBook.jsp"/>
    <button type="submit" class="btn btn-secondary" name="action" value="add" >Add</button>
    <button type="submit" class="btn btn-secondary" name="action" value="cancel">Cancel</button>
</form>

</body>
</html>
