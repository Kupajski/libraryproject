<%--
  Created by IntelliJ IDEA.
  User: Bartosz
  Date: 13.09.2018
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
<jsp:include page="/WEB-INF/header.jsp"/>
<form class="navbar-dark" action="servlets.LoginUserServlet" method="post">
    <div class = "row">
        <div class = "col">
            <div class="form-group">
                <label for="login">Username</label>
                <input type="text" class="form-control" id="login" name="login"  placeholder="login">
            </div>
        </div>

        <div class = "col">
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Zaloguj</button>
</form>
</body>
</html>
