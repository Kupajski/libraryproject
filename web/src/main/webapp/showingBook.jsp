<%--
  Created by IntelliJ IDEA.
  User: Bartosz
  Date: 18.09.2018
  Time: 08:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <title>Book</title>
    <link rel="stylesheet" href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
<jsp:include page="WEB-INF/header.jsp"/>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">${requestScope.book.title}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${requestScope.book.author_id.first_name} ${requestScope.book.author_id.last_name}</h6>
                    <p class="card-text">${requestScope.book.summary}</p>
                </div>
            </div>
        </div>
        <div class="col">
            <c:choose>
                <c:when test="${requestScope.book.borrow == true}">
                    Book is borrowed.
                <form action="/servlets.borrowingActionServlet" method="post">
                        <input type="hidden" name="idBookReturn" id="idBookReturn" value=${requestScope.book.id_book}>
                        <button type="submit" class="btn btn-secondary" name="action" value="Return"> Return</button>
                </form>
                </c:when>
                <c:otherwise>
                    You Can borrow it.
                    <form action="/servlets.borrowingActionServlet" method="get">
                            <input type="hidden" name="idBookBorrow" id="idBookBorrow" value=${requestScope.book.id_book}>
                            <button type="submit" class="btn btn-secondary" name="action" value="Borrow">Borrow</button>
                    </form>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

</div>

</body>
</html>
