<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
        <a class="navbar-link " >
           <!--<c:choose>
                <c:when test="${not empty param.username}">
                    ${param.username}
                </c:when>
                <c:otherwise>
                    Uzytkownik niezalogowany
                </c:otherwise>
            </c:choose>-->
            Library
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <c:choose>
                        <c:when test="${empty sessionScope.login}">
                            <a class="nav-link" href="login.jsp">Login</a>
                        </c:when>
                            <a class="nav-link" href="/servlets.LogoutServlet">Logout</a>
                    </c:choose>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="servlets.HomeServlet">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="register.jsp">Register</a>
                </li>
            </ul>
        </div>
    </div>
</nav>