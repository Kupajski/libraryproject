<div class="row justify-content-around">

    <div class="form-group col-md-6">
        <label for="bookTitle">Title</label>
        <input type="text" class="form-control" id="bookTitle" placeholder="Enter book title" name="bookTitle" value=${requestScope.book.title}>
    </div>
    <div class="form-group col-md-6">
        <label for="bookCategory">First name</label>
        <input type="text" class="form-control" id="bookCategory" placeholder="Enter Category "
               name="bookCategory" value=${requestScope.book.category}>
    </div>
</div>
<div class="row justify-content-around">
    <div class="form-group col-md-3">
        <label for="first_name">First name</label>
        <input type="text" class="form-control" id="first_name" placeholder="Enter Author's first name"
               name="first_name" value=${requestScope.book.author_id.first_name}>
    </div>
    <div class="form-group col-md-3">
        <label for="last_name">last name</label>
        <input type="text" class="form-control" id="last_name" placeholder="Enter Author's last name"
               name="last_name" value=${requestScope.book.author_id.last_name}>
    </div>
    <div class="form-group col-md-6 ">
        <label for="isbn">ISBN</label>
        <input type="number" class="form-control" id="isbn" placeholder="Enter ISBN number" name="isbn" value=${requestScope.book.isbn}>
    </div>
</div>
<div class="row justify-content-around">
    <div class="form-group col-md-6">
        <label for="bookReleaseDate">Release date</label>
        <input type="text" class="form-control" id="bookReleaseDate" placeholder="Enter release date"
               name="bookReleaseDate" value=${requestScope.book.release_date}>
    </div>
    <div class="form-group col-md-6">
        <label for="bookPages">Pages</label>
        <input type="number" class="form-control" id="bookPages" placeholder="Enter number of pages"
               name="bookPages" value=${requestScope.book.pages}>
    </div>
</div>
<div class="row justify-content-around">
    <div class="form-group col-md-12">
        <label for="bookSummary">Summary</label>
        <textarea class="form-control" id="bookSummary" placeholder="Enter summary" name="bookSummary" value=${requestScope.book.summary}></textarea>
    </div>
</div>
<input type="hidden" name="id_book" id="id_book" value=${requestScope.book.id_book}>
