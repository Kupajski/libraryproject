import org.junit.Test;
import repositories.AuthorDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class entityStart {

    @Test
    public void entityStart(){
        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        AuthorDao authorDao = new AuthorDao(entityManager);
    }
}
