package repositories;
import models.Book;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


public class BookDao extends GenericDao<Book,Integer> {

    public BookDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Book create(Book entity) {
        return super.create(entity);
    }

    @Override
    public Book read(Integer id) {
        return super.read(id);
    }

    @Override
    public Book update(Book entity) {
        return super.update(entity);
    }

    @Override
    public void delete(Book entity) {

            super.delete(entity);

    }

    public boolean checkIfBookIsAlreadyBorrowed(Integer id_book){

        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select b from Borrow b where b.id_book = :book");
            query.setParameter("book", id_book);
            if(query.getSingleResult()!=null){
                return true;
            }
            transaction.commit();
        } catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return false;
    }

    public List<Book> findAll(){
        EntityTransaction transaction = null;
        List<Book> booksList = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select b from Book b");
            booksList = query.getResultList();
            transaction.commit();
        }catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return booksList;
    }

}
