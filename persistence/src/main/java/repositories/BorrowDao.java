package repositories;

import models.Borrow;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

public class BorrowDao extends GenericDao<Borrow,Borrow> {

    protected BorrowDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Borrow create(Borrow entity) {
        if(checkIfBookIsAlreadyBorrowed(entity.getBook_id().getId_book())){
            //You cannot borrow book that is already borrowed
            return null;

        }else{
            return super.create(entity);
        }
    }

    @Override
    public void delete(Borrow entity) {
        super.delete(entity);
    }

    public boolean checkIfBookIsAlreadyBorrowed(Integer id_book){

        EntityTransaction transaction = null;

        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select b from Borrow b where b.id_book = :book");
            query.setParameter("book", id_book);
            if(query.getSingleResult()!=null){
                return true;
            }
            transaction.commit();
        } catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return false;
    }
}
