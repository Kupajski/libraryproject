package repositories;

import models.Author;
import models.Book;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class AuthorDao extends GenericDao<Author,Integer> {

    public AuthorDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Author create(Author entity) {
        return super.create(entity);
    }

    @Override
    public Author read(Integer id) {
        return super.read(id);
    }

    @Override
    public Author update(Author entity) {
        return super.update(entity);
    }

    @Override
    public void delete(Author entity) {
        super.delete(entity);
    }

    public Author findAuthor(String first_name, String last_name){

//        EntityTransaction transaction = null;
//        Author authorFound = null;
//        Author authorWithId = null;

            Query query = em.createQuery("select author from Author author where author.first_name = :first_name and author.last_name = :last_name ");
            query.setParameter("first_name", first_name);
            query.setParameter("last_name", last_name);

//            if(query.getResultList().isEmpty()){
//                Author author = new Author(first_name,last_name);
//                AuthorDao authorDao = new AuthorDao(em);
//                authorWithId = authorDao.create(author);
//                return authorWithId;
//            }else{
//                authorFound =(Author) query.getSingleResult();
//
//            }
        try{
            Author author = (Author) query.getSingleResult();
            return author;
        }catch(Exception e){
            return null;
        }

    }
}
