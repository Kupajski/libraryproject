package repositories;

import models.Borrow;
import models.Borrower;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class BorrowerDao extends GenericDao<Borrower,Integer> {

    public BorrowerDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Borrower create(Borrower entity) {
        return super.create(entity);
    }

    @Override
    public Borrower read(Integer id) {
        return super.read(id);
    }

    @Override
    public Borrower update(Borrower entity) {
        return super.update(entity);
    }

    @Override
    public void delete(Borrower entity) {
        super.delete(entity);
    }

    public boolean findByLogin(String login) {

        Query query = em.createQuery("select borrower from Borrower borrower where  borrower.login = :login ");
        query.setParameter("login", login);

        try{
            Borrower borrower = (Borrower) query.getSingleResult();
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public boolean loginUser(String login, String password){


        return false;
    }

}
